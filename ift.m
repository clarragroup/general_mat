
function [a,b] = ift(x,y,sidesin,sidesout)

% function [a,b] = ift(x,y,sidesin,sidesout)
%
% ifft y, where y=f(x)
% get y = ifft(yft), and xft
% where y = f(x)
%
%
% sides = 1 for one-sided (will be made two-sided before FT)
%			note: for one-sided can have any number of points
%			assumes that the spectrum starts from 0 frequency
%
% Note: the format must be as follows:
%
% 	for single sided:
% 		spectrum must start with zero frequency and end with
%       nyquist critical frequency.  It can have even or odd
%       number of points (will end up even)
%
%	for double sided:
%		must be in following format already ...
%		zero frequency up to nyquist frequency, then
%		negative of one less than nyquist frequency down to
%		negative of one more than zero frequency
%
%   to optimize we should use a factor of 2 number of points for
%       double-sided but in this code we use factor of 2 number of
%       points plus one point (zero nu)
%       this should be fixed in future versions.
%
%
% From "help fft" in matlab:
%   The functions Y=fft(x) and y=ifft(X) implement the transform
%   and inverse transform pair given for vectors of length N by:
%
%   For length N input vector x, the DFT is a length N vector X,
%   with elements
%                    N
%      X(k) =       sum  x(n)*exp(-j*2*pi*(k-1)*(n-1)/N), 1 <= k <= N.
%                   n=1
%   The inverse DFT (computed by IFFT) is given by
%                    N
%      x(n) = (1/N) sum  X(k)*exp( j*2*pi*(k-1)*(n-1)/N), 1 <= n <= N.
%                   k=1
%
%   See also FFT2, FFTN, FFTSHIFT, FFTW, IFFT, IFFT2, IFFTN.
%
%
% Inverse Fourier Transform
% Penny Rowe
% July 26, modified March 3, 2000 and March 26, 2000
%   Oct. 2016
%   From old version ift3.m, renamed and keeping track of version
%   with git



% make y vector into row vectors
dims = size(y);
if dims(1)==1
  % already row vector
elseif dims(2)==1
  y=y';
end


% Note: assuming we start with an interferogram and fft to get a spectrum
% or alternatively start with a spectrum and ifft to get an interferorgram
% Then the following is true
%
% spectrum(1) = sum(interferogram)
% interferogram(1) = sum(spectrum)/10
%
% So, in going from the spectrum to the interferogram (ift), we want
% the integral under the spectrum to equal the integral under the
% interferogram, so we make the first point be the integral under the
% spectrum


if sidesin == 1
  
  % Flip to make the left hand side (0.032s)
  rhs = fliplr(y(2:end-1)); % replaced length(y) with end
  
  % note: the spectrum must be anti-symmetric! (0.030s)
  if isreal(y)
    y = [y rhs];
  else
    y = [real(y)-1i*imag(y) rhs];
  end
  
  
  % Note: we have 2 x number of points - 2
  
  
elseif sidesin ==2
  x = x(x>=0);
  % donothing
else
  error('sidesin =1 for one-sided, 2 for 2-sided');
end


% I assume everything is perfect already
% Now ifft using Matlab's built in code (e.g. 0.19s)
b = ifft(y);


if sidesout==2
  % make a a vector of integers, starting from zero (0.18s)
  N = 2*length(x)-2;
  n = (0:length(x)-1);
  n = [n -fliplr(n(2:end-1))]; %replaced length(n) with end
  dx = x(2)-x(1);

  % now multiply by da to scale properly
  a = (1/(N*dx)) * n;
  
elseif sidesout==1
  % make a a vector of integers, starting from zero (0.18s)
  N = length(x);
  dx = x(2)-x(1);
  
  % now multiply by da to scale properly
  a = (1/((2*N-2)*dx)) * (0:N-1);
  b = b(1:N);
  
end







