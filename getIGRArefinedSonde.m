
DirOut = '/Volumes/Data/Projects/P5_AntarcticClouds/IMG/Work/SondeOverlaps/sondes';

% ---------------------
% Data Record Format:
% ---------------------
%
%
%   Variable Name            Columns  Description
%   -----------------------  -------  -----------
%
%   Major Level Type           1-  1  1 = standard pressure level
%                                     2 = significant thermodynamic level
%                                     3 = additional wind level
%
%   Minor Level Type           2-  2  1 = surface, 2 = tropopause, 0 = other
%
%
%   Pressure                   3-  8  units of Pa (mb * 100)
%
%   Pressure Flag              9-  9  A, B, or blank (see note 4 above)
%
%   Geopotential Height       10- 14  units of meters
%
%   Geopotential Height Flag  15- 15  A, B, or blank (see note 4 above)
%
%   Temperature               16- 20  units of degrees C * 10
%
%   Temperature Flag          21- 21  A, B, or blank (see note 4 above)
%
%   Dewpoint Depression       22- 26  units of degrees C * 10
%
%   Wind Direction            27- 31  units of degrees (0-360, inclusive)
%
%   Wind Speed                32- 36  units of (m/s)*10



% Fix it up
iskipped=[];
cd(DirOut)
dirfnames = dir;
for ifile=1:length(dirfnames)
  fname = dirfnames(ifile).name;
  if length(fname)==20 && strcmpi(fname(1:5),'sonde')
    
    % Rename with beginning "refined_"
    fnameout = ['refined_' fname];
    load(fname)
    
    % Remove unneeded levels
    iout = find(sonde.pressure==-9999 & sonde.height==-9999);
    iout = union(iout,find(sonde.temp==-9999 & sonde.dewpointDepr==-9999));
    sonde.pressure(iout)=[];
    sonde.height(iout)=[];
    sonde.dewpointDepr(iout)=[];
    sonde.temp(iout)=[];
    sonde.windDir(iout)=[];
    sonde.windSpeed(iout)=[];
    
    % Replace -9999 with NaN
    if any(sonde.pressure==-9999)
      error('Bad pressures, check out this sounding.');
    end
    
    sonde.temp(sonde.temp==-9999)=NaN ;
    sonde.dewpointDepr(sonde.dewpointDepr==-9999)=NaN ;
    sonde.height(sonde.height==-9999)=NaN ;
    
    if isempty(sonde.height) || isnan(sonde.height(1))
      fprintf(['skipping ifile ' num2str(ifile) '\n']);
      iskipped = [iskipped ifile];
    else
      
      
      % dewpoint depression = T-Td
      % This means that the dewpoint temperature is Td = T - dewpoint dep.
      sonde.height   = sonde.height/1000;        % km
      sonde.dewpoint = sonde.temp/10 - sonde.dewpointDepr/10 ;
      sonde.temp     = sonde.temp/10 + 273.15 ;  % K
      sonde.pressure = sonde.pressure/100;   % mb
      sonde.RHw      = humidRS80(sonde.dewpoint,sonde.temp,2,4);
      sonde.Pw       = humidRS80(sonde.dewpoint,sonde.temp,2,1);
      
      
      % Fill in the missing heights
      % We need to be sure RHw is a real number
      RHw = sonde.RHw ;
      
      out = sonde.height;
      ibad = find(isnan(sonde.height));
      for iout=1:length(ibad);
        ii = ibad(iout);
        if isnan(RHw(ii)) 
          if isnan(RHw(ii-1))
          % Use the next closest RHw
          ind = find(isnan(RHw(1:ii-1))==0,1,'last');
          RHw(ii-1) = RHw(ind) ;
          end
          RHw(ii)   = RHw(ii-1) ;
        elseif isnan(RHw(ii-1))
          RHw(ii-1)=RHw(ii) ;
        end
        z = hypsometric(sonde.pressure(ii-1:ii),sonde.temp(ii-1:ii),...
          RHw(ii-1:ii),out(ii-1)*1000,sonde.pressure(ii-1:ii),'P') ;
        out(ibad(iout)) = z(2)*1e-3;
      end
      
      %plot(sonde.pressure,out)
      sonde.height = out ;
      
      
      
      % clean up a little
      sonde = rmfield(sonde,{'pressureFlag', 'tempFlag','heightFlag'});
      sonde = rmfield(sonde,{'levelType','dewpointDepr'});
      
      % check for NaNs in temperature field
      if any(isnan(sonde.temp))
        error('Bad temperatures, check out this sounding.');
      end
      
      save(fnameout,'sonde');
      
    end
  end
end
  % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
  

