function output = attribute_list(cdfFile)
% C Cox
% 1/31/2010
%
% Ex. in current directory: output = attRet('thenetcdf.nc')
%
% Retrieves list of attributes from a netcdf file and prints to screen.
% Intended as a Matlab netcdf toolbox compatable update to 'ncbrowser'.
%
% 'output' is a structure containing all variables in the netcdf + a new 
% one called 'global'. Each variable is a structure of all it attributes
% containing each of their internal values. 'global' includes the global
% attributes.
%
% ** if attribute name begins with illegal character (ie. '_attname'),
% 'varname_' will begin the variable name.
%
% UPDATE: 7/31/12: ability to write varname beginning with numeric - ccox
% UPDATE: 10/8/2014: general

import_netcdf

ncid = netcdf.open(cdfFile,'NC_NOWRITE');

% How many attributes?
[numdims numvars numatts unlimdimID] = netcdf.inq(ncid);

% Vars
for k = 1:numvars % # variables
	[varname vartype vardimIDs varatts] = netcdf.inqVar(ncid,k-1);
    for h = 1:varatts % # attributes
        gattname = netcdf.inqAttName(ncid,k-1,h-1);
        gattval  = netcdf.getAtt(ncid,k-1,gattname);
        if ~isletter(gattname(1)); gattname = ['varname_' gattname]; end % **
        output.(varname).(gattname) = gattval;
    end
end

% % Globals
for k = 1:numatts
    gattname = netcdf.inqAttName(ncid,netcdf.getConstant('NC_GLOBAL'),k-1);
	gattval  = netcdf.getAtt(ncid,netcdf.getConstant('NC_GLOBAL'),gattname);
    output.globals.(gattname) = gattval;
end