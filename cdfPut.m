function cdfPut(Var,thename,prec,Att,dimid,ncid)
import_netcdf
netcdf.reDef(ncid);
varid = netcdf.defVar(ncid,thename,prec,dimid);
netcdf.endDef(ncid);
netcdf.putVar(ncid,varid,Var');
netcdf.reDef(ncid);
if ~isempty(Att);
  Attv = struct2cell(Att);
  Attn = fieldnames(Att);
  for h = 1:length(Attn)
    netcdf.putAtt(ncid,varid,char(Attn(h)),char(Attv(h)));
  end
end
netcdf.endDef(ncid);
