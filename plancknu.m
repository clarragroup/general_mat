function f = plancknu(nu_icm,T)

% function f = plancknu(nu_icm,T);
%
% spectral Planck function as function of wavenumbers (cm-1)
%
% [h]    = J*s
% [c]    = m/s
% [cbar] = cm/s
% [k]    = J*K-1
%
%
%    Note: LBLRTM uses ...
%c    Constants from NIST 01/11/2002
%
%      DATA PLANCK / 6.62606876E-27 /, BOLTZ  / 1.3806503E-16 /,
%     *     CLIGHT / 2.99792458E+10 /, 
%
% Notes: Can now take a vector of wavenumbers (nu_icm) AND a vector of
%  temperatures (T) to produce a matrix f. (PMR Sept. 20, 2014)
%

h    = 6.62606896e-34;				% J s;  CODATA 2006
c    = 2.99792458e8;				% m/s;  NIST
k    = 1.3806504e-23;				% J K-1; CODATA 2006
cbar = 100*c;			  		% cm/s

% Make nu_icm into a column vector, T into a row vector
nulen = length(nu_icm);
Tlen  = length(T);
if size(nu_icm,2) ~= nulen; nu_icm = nu_icm'; end  % Row vector
if size(T,1) ~= Tlen; T=T'; end                    % Column vector


% QC on inputs
if size(nu_icm,1)~=1; error('wavenumber must be scalar or vector'); end
if size(T,2)~=1; error('temperature vector must be scalar or vector'); end


top     = 2 * h * cbar^3 .* nu_icm.^3;
hcbarnu = h*cbar*nu_icm ;
kT      = k*T ;

if nulen==1 || Tlen==1
  bottom =  c^2*  ( exp(hcbarnu./kT)-1 );
  f      = cbar*top./bottom;
elseif nulen > Tlen
  % Loop over T
  f = zeros(Tlen,nulen);
  for iT = 1:length(T)
    bottom =  c^2*  ( exp(hcbarnu./kT(iT))-1 );
    f(iT,:) = cbar*top./bottom;
  end
    
elseif nulen <Tlen
  % Loop over wavenumber
  f = zeros(Tlen,nulen);
  for inu=1:length(nu_icm)
    bottom =  c^2*  ( exp(hcbarnu(inu)./kT)-1 );
    f(:,inu) = cbar*top(inu)./bottom;
  end  
end


% now set f to zero for wavenumbers less than or equal to 0
f(:,nu_icm<=0)=0;


%[B]= cm*s-1*J*s*cm3*s-3*cm-3*m-2*s2
%[B]= W m-2 cm



	