function theResult = rd_netcdf(theNetCDFFile, varargin)

% ... If using Octave, must import the netcdf module
if (is_octave)
  % Octave
  import_netcdf
end

%
% function theResult = rd_netcdf(theNetCDFFile, varargin)
% rd_netcdf -- Load NetCDF variables.
%  rd_netcdf('theNetCDFFile', 'var1', 'var2', ...) loads the
%   given variables of 'theNetCDFFile' into the Matlab
%   workspace of the "caller" of this routine.  If no names
%   are given, all variables are loaded.  The loaded names
%   are returned or assigned to "ans".  No attributes are
%   loaded.
% 
% Copyright (C) 1997 Dr. Charles R. Denham, ZYDECO.
%  All Rights Reserved.
%   Disclosure without explicit written consent from the
%    copyright owner does not constitute publication.
%
%
% version of DCT 9/29/97 to return all variables in a structure
% and to return fields w/ multiple dimensions.
%
% edited by Von P. Walden on 081114 to use new netcdf toolbox in Matlab
% 2008b.
%
% Modified by Penny Rowe on 2015/03/05 to check if running Octave,
%   and, if so, import the netcdf module
%

if nargin < 1, help(mfilename), return, end

result = [];
if nargout > 0, theResult = result; end

% get # of dimensions, # of variables, # of attributes,
% and the record dimension
f = netcdf.open(theNetCDFFile, 'NC_nowrite');

if isempty(f), return, end

% get variable names
if isempty(varargin) 
    [ndims,nvars,natts,unlimdimID] = netcdf.inq(f); 
    for k = 1:nvars
        varargin{k} = netcdf.inqVar(f,k-1);
        eval(['result.' varargin{k} ' = netcdf.getVar(f,k-1);']);
    end
else
	nvars = length(varargin);
	for k = 1:nvars
		varid = netcdf.inqVarID(f,varargin{k});
		eval(['result.' varargin{k} ' = netcdf.getVar(f,varid);']);
	end
end
 
netcdf.close(f)

theResult = result;
