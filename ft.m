
function [nu, yft] = ft(x,y,sidesin,sidesout)

% function [nu, yft] = ft(x,y,sidesin,sidesout)
%
% fft y, where y=f(x)
% get yft = fft(y), and nu
% where yft = f(nu)
%
% sides = 1 for one-sided (will be made two-sided)
% sides = 2 for two-sided
%
% Fourier Transform
% Penny Rowe
% July 26,
%
% Modifications:
%   March 3, 2000
%   June 6, 2013
%   Oct. 2016: Compared to ft.m and cleaned up
%   From old version ft3.m, renamed and keeping track of version
%   with git



% make vectors into row vectors
dims = size(x);
if dims(1)==1
  % already row vector
elseif dims(2)==1
  x=x';
end

dims = size(y);
if dims(1)==1
  % already row vector
elseif dims(2)==1
  y=y';
end


if sidesin == 1
  y2 = fliplr(y(2:end-1));
  y = [y y2]; % original

  % We have 2*length(y)-2 points, so if N is a power of 2,
  % we should have started with N+1 points, to give
  % 2*(N+1)-2 = 2N points

  % The following appears not to work:
  %y2 = fliplr(y(2:length(y)-1));
  %rhs = fliplr(y(2:length(y)-1));
  %y = [rhs real(y)-1i*imag(y) ];

elseif sidesin ==2
  % get positive values of x only
  x = x(x>=0);
else
  fprintf('error: sidesin =1 for one-sided, 2 for 2-sided');
  return
end

yft = fft(y);


if sidesout==2
  % make a a vector of integers, starting from zero
  N = 2*length(x)-2;
  n = (0:length(x)-1);
  n = [n -fliplr(n(2:end-1))];
  dx = x(2)-x(1);
  
  nu = (1/(N*dx)) * n;
elseif sidesout==1
  % make a a vector of integers, starting from zero
  N   = length(x);
  dx  = x(2)-x(1);
  nu  = (1/((2*N-2)*dx)) * (0:N-1);
  yft = yft(1:N);
end

