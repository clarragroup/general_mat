function out = hypsometric(in,T,RH,Zo,Po,inisZ_P)

% Inputs
% in: height or pressure in mb
% T: temperature in Kelvin
% RH: relative humidity (%)
% Zo: surface altitude (meters)
% Po: surface pressure 
% inisZ_P (character string, either 'z' or 'P'), to tell whether
%   in is height or pressure
%
%
% code written by Steve H for hypsometric determination
% of altitude (in meters). 
%
% Modified by PMR to take either Z or P as input
%
% Warning: this is not very accurate, just approximate.


% The hydrostatic equation:
% 
% P1/P2 = exp[ g/RT * (z2-z1) ]
% P2    = P1 * exp[ g/RT * (z2-z1) ]


% constants
Rd = 287.058;  % 
g = 9.80665;

if strcmpi(inisZ_P,'z') ;
    % input is z, output needs to be P
    
    eps = 0.622;
    Z = in ;
    P = zeros(size(Z)) + Po ;

    e = RH./100.*esw(T);  % I assume wrt water, but should be?

    for i = 1:length(T)-1
      dP = 1e6;
      while abs(dP) > .1
        Pprev = P(i+1);
        w = eps.*(e./(P-e));
        Tv = T.*((1+(w./eps))./(1+w));
        TvBar = (Tv(i)+Tv(i+1))/2;
        dz = Z(i+1)-Z(i);
        % P1 = P(i);
        % P2 = P(i+1);
        % dz = (Rd*TvBar/g)*log(P1/P2);
        % log(P1/P2) = dz ./ (Rd*TvBar/g)
        % P1/P2 = exp( dz ./ (Rd*TvBar/g) )
        % P1 = P2*exp( dz ./ (Rd*TvBar/g) )
        % P2 = P1 exp( -dz ./ (Rd*TvBar/g) )
        P(i+1:end) = P(i)*exp( -dz ./ (Rd*TvBar/g) ) ;  %disp(P(i+1));
        dP = P(i+1)-Pprev; %disp(dP); disp('');
      end

    end

    out = P ;



elseif strcmpi(inisZ_P,'P')
    % Input is P, output needs to be Z

    eps = 0.622;
    P = in ;
    Z = 0*T;
    Z(1) = Zo;

    %e = (RHI./100).*(10.^((-2663.5./T) + 12.537));
    e = RH./100.*esw(T);  % I assume wrt water, but should be?
    w = eps.*(e./(P-e));
    Tv = T.*((1+(w./eps))./(1+w));

    for i = 1:length(T)-1
        TvBar = (Tv(i)+Tv(i+1))/2;
        P1 = P(i);
        P2 = P(i+1);
        dz = (Rd*TvBar/g)*log(P1/P2);
        Z(i+1) = Z(i) + dz;
    end
    
    out = Z ;

else
    error('Bad value for isZ_P');
end



function [es] = esw(T)

%input pressure in millibar, temperature in kelvin.  output of esw in millibars

%using intergrated clausius-clapyeron equation for h2o mixing ratio
a = 17.27;
b = 35.86;
es = (3.8./0.62197).*exp(a.*((T-273)./(T-b)));

