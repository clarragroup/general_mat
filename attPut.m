function attPut(thename,Att,ncid)
import_netcdf
netcdf.reDef(ncid);
varid = netcdf.getConstant('NC_GLOBAL');
netcdf.putAtt(ncid,varid,thename,Att);
netcdf.endDef(ncid);