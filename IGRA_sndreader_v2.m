
%function [sondeFiles, sonde] = IGRA_sndreader(IgraSondeFname,DirIn,DirOut)

% Purpose:
%	Load in an IGRA sounding file containing many soundings
%	and output a separate sounding file for each sounding.
%
% Notes:
%	This used to be a script.  The old variables are commented out below.
%
% Output:
%	sondeFiles: char array of output filenames of form
%		['sonde' sonde.date '_' sonde.releaseTime(1:2)];
%
%	sonde: structured array with fields: station, date, obsHour, releaseTime,
%		noLevels, levelType, pressure, pressureFlag, height, heightFlag,
%		temp, tempFlag, dewpoint, windDir, windSpeed
%
%   dependencies:
%     function calls, including nested:
%       humidRS80.m
%         hypsometric.m
%
% by Penny M. Rowe
%
%
% current as of 2 July 2007
% major updates 5 Oct. 2009
% major updates 6 Nov. 2016 - for new version of IGRA, v2
%
% Notes: these are saved by obsHour, rather than releaseTime,
%   I think this ensures that they are *not* off by a day!
%
%
% Last run with the following inputs:
%	  IgraSondeFname='YEU_71917_2006_2008.txt';
%     DirIn = '/Volumes/Data/Data/Eureka/Igra/';
%	  DirOut = '/Volumes/Data/Data/Eureka/IGRA_sonde2';
%


addpath /Users/prowe/Git_repos/general_mat

if exist([DirIn IgraSondeFname],'file')~=2
  error(['File ' IgraSondeFname ' not found in ' DirIn]);
end

fid = fopen([DirIn IgraSondeFname],'r');
notdone=1;
isnd=0;
while notdone
  isnd=isnd+1;
  clear sonde
  headertxt = textscan(fid,'%s %s %s %s %s %s %s %s %s %s',1);
  header = char(headertxt{1});
  if isempty(header)
    notdone=0;
    fprintf('Done reading files.\n');
  elseif ~strcmp(header(1),'#')
    error(['Expected # sign, but instead got ' header(1)]);
  else
    sonde.station = header(2:12) ;
    sonde.year        = (char(headertxt(2))); %header(14:17) ;
    sonde.month       = (char(headertxt(3))); %header(19:20);
    sonde.day         = (char(headertxt(4))); %header(22:23);
    sonde.obsHour     = (char(headertxt(5))); %header(25:26);
    sonde.releaseTime = str2num(char(headertxt(6))); %header(28:31);
    sonde.noLevels    = str2num(char(headertxt(7))); %header(33:36);
    %sonde.P_src    = headertxt(8); %header(38:45);
    %sonde.NP_src   = headertxt(9); %header(47:54);
    %sonde.lat      = headertxt(10); %header(56:62);
    %sonde.lon      = headertxt(11); %header(64:71) ;

    %sonde.noLevels = str2num(char(headertext{2}));
    
    for ilevel=1:sonde.noLevels
      sondeArray = fgets(fid); %textscan(fid,'\n',1); %%[^\n]',1) ;
      %sondeArray = char(cellstr{1}) ;
      
      sonde.levelType(ilevel,1:2)  = sondeArray(1:2);
      sonde.etime(ilevel,1:5)      = str2num(sondeArray(4:8));
      sonde.pressure(ilevel,1)     = str2num(sondeArray(10:15));
      sonde.pressureFlag(ilevel,1) = sondeArray(16);
      sonde.height(ilevel,1)       = str2num(sondeArray(17:21));
      sonde.heightFlag(ilevel,1)   = sondeArray(22);
      sonde.temp(ilevel,1)         = str2num(sondeArray(23:27));
      sonde.tempFlag(ilevel,1)     = sondeArray(28);
      sonde.rh(ilevel,1)           = str2num(sondeArray(29:33));
      sonde.dewpointDepr(ilevel,1) = str2num(sondeArray(35:39));
      sonde.windDir(ilevel,1)      = str2num(sondeArray(41:45));
      sonde.windSpeed (ilevel,1)   = str2num(sondeArray(47:51));

    end
      
      

    % See igra2-data-format.txt


    % Fix it up

    % Remove unneeded levels
    iout = find(sonde.pressure==-9999 & sonde.height==-9999);
    iout = union(iout,find(sonde.temp==-9999 & sonde.dewpointDepr==-9999));
    sonde.pressure(iout)=[];
    sonde.height(iout)=[];
    sonde.dewpointDepr(iout)=[];
    sonde.temp(iout)=[];
    sonde.windDir(iout)=[];
    sonde.windSpeed(iout)=[];
    
    
    sonde.temp(sonde.temp==-9999)=NaN ;
    sonde.dewpointDepr(sonde.dewpointDepr==-9999)=NaN ;
    sonde.height(sonde.height==-9999)=NaN ;
    
    if ~any(sonde.pressure) || ~any(sonde.height) || isnan(sonde.height(1))
      fprintf(['skipping ifile ' num2str(ifile) '\n']);
      iskipped = [iskipped ifile];
    else
      
      
      % dewpoint depression = T-Td
      % This means that the dewpoint temperature is Td = T - dewpoint dep.
      % A function call is made here to humidRS80.m
      % and humidRS80.m calls hypsometric.m
      sonde.height   = sonde.height/1000;        % km
      sonde.dewpoint = sonde.temp/10 - sonde.dewpointDepr/10 ;
      sonde.temp     = sonde.temp/10 + 273.15 ;  % K
      sonde.pressure = sonde.pressure/100;   % mb
      sonde.RHw      = humidRS80(sonde.dewpoint,sonde.temp,2,4);
      sonde.Pw       = humidRS80(sonde.dewpoint,sonde.temp,2,1);
      
      
      % Fill in the missing heights
      % We need to be sure RHw is a real number
      RHw = sonde.RHw ;
      
      out = sonde.height;
      ibad = find(isnan(sonde.height));
      for iout=1:length(ibad);
        ii = ibad(iout);
        if isnan(RHw(ii)) 
          if isnan(RHw(ii-1))
            % Use the next closest RHw
            ind = find(isnan(RHw(1:ii-1))==0,1,'last');
            RHw(ii-1) = RHw(ind) ;
          end
          RHw(ii)   = RHw(ii-1) ;
        elseif isnan(RHw(ii-1))
          RHw(ii-1)=RHw(ii) ;
        end
        z = hypsometric(sonde.pressure(ii-1:ii),sonde.temp(ii-1:ii),...
          RHw(ii-1:ii),out(ii-1)*1000,sonde.pressure(ii-1:ii),'P') ;
        out(ibad(iout)) = z(2)*1e-3;
      end
      
      %plot(sonde.pressure,out)
      sonde.height = out ;
      
      
      
      % clean up a little
      try
        sonde = rmfield(sonde,{'pressureFlag', 'tempFlag','heightFlag'});
        sonde = rmfield(sonde,{'levelType','dewpointDepr'});
      catch
      end
      
      % check for NaNs in temperature field
      if any(isnan(sonde.temp))
        error('Bad temperatures, check out this sounding.');
      end
      

    end
    
    
    sondeFile = ['sonde' sonde.year sonde.month sonde.day '_' sonde.obsHour '.mat'];
    sondeFiles(isnd,:) = sondeFile ;

    % Now we need to make a netcdf file
    
    save([DirOut sondeFile],'sonde')
    
  end
  
  
end

fclose(fid);


