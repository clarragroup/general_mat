function createDumNetcdf(filename,sname,clobber)

% Purpose: To create a netcdf file from a structured array
%
% Inputs:
%  filename: The output netcdf file name.
%  sname:    The structured array.
%  clobber:  Write over existing output file, if there?
%
% Outputs: 
%  


% .. If using Octave, netcdf is not built in but must be imported
if (is_octave)
  %pkg load netcdf  % In octaverc file
  import_netcdf
end


%NC_BYTE,
%NC_CHAR, NC_SHORT, NC_INT, NC_FLOAT, or NC_DOUBLE

if clobber==1
  clobStr = 'NC_SHARE';
else
  clobStr = 'NC_NOCLOBBER';
end

% Open the netcdf file
ncid = netcdf.create(filename,clobStr);

% Get the field names
%fnames = fields(sname) ;           % Old, incompatible with Octave, PMR, 2016/14/03
fnames = fieldnames(sname) ;        % Should work with Octave/Matlab, PMR, 2016/14/03
dimcount = 0 ;
Nlens = zeros(length(fnames),1);
% Loop over the field names
for i = 1:length(fnames)

  fname = char(fnames(i)) ;
  Nclass = class(sname.(fname)) ;
  
  % Avoid "single" for now, since it does not work
  if strcmp(Nclass,'single')
    Nclass = 'double';
    sname.(fname) = double(sname.(fname));
  
  elseif strcmpi(Nclass,'int8')
    Nclass = 'nc_int';
    
  elseif strcmpi(Nclass,'int32')
    Nclass = 'nc_int';
    
    % Special logic for cell arrays, which must be converted to
    % character arrays and a vector giving the length of each string.
  elseif strcmp(Nclass,'cell')
    
    Nclass = 'char';                   % We will convert to string array
    %
    %     % Get the length of each string in the cell array
    %     maxlen=0;
    %     for k=1:length(sname.(fname))
    %       disp(k);
    %       stringarraylens(k)=length(sname.(fname)(k)) ;
    %     end
    %
    stringarray = char(sname.(fname)) ;   % Get the character array
    sname.(fname) = stringarray ;
    %     [Nlen,Nwid] = size(stringarray) ;     % and it's size
    %
    %     isvector = 0 ;
    %     if Nlen==1 || Nwid==1
    %       isvector=1;
    %     end
    %
    %
    %    % Matlab will put the fastest-varying dimension first, so ...
    %     if Nwid>Nlen && Nlen>1                % flip if needed
    %       Nwid0=Nwid;
    %       Nwid = Nlen;
    %       Nlen = Nwid0; clear Nwid0
    %       %stringarray = stringarray';
    %     end
    %
    %     idim = find(Nwids==Nwid,1,'first') ;
    %     if ~isempty(idim)
    %       % We already got this dimension, so just use it
    %       dimname = ['dim' num2str(idim)];
    %       dimid   = netcdf.inqDimID(ncid,dimname) ;
    %     else
    %       % Get the dimensions
    %       dimcount=dimcount+1;
    %       dimname = ['dim' num2str(dimcount)];
    %       dimid = netcdf.defDim(ncid,dimname,Nwid) ;
    %       Nwids(dimcount) = Nwid ;
    %     end
    %
    %     if isvector
    %       % Define a new variable in the file.
    %       varid = netcdf.defVar(ncid,fname,Nclass,dimid) ;
    %     else
    %       idim2 = find(Nwids==Nlen,1,'first') ;
    %       if ~isempty(idim2)
    %         % We already got this dimension, so just use it
    %         dimname2 = ['dim' num2str(idim)];
    %         dimid2   = netcdf.inqDimID(ncid,dimname2) ;
    %       else
    %         % Get the dimensions
    %         dimcount=dimcount+1;
    %         dimname2 = ['dim' num2str(dimcount)];
    %         dimid2 = netcdf.defDim(ncid,dimname2,Nwid) ;
    %         Nwids(dimcount) = Nlen ;
    %       end
    %
    %       % Define a new variable in the file.
    %       varid = netcdf.defVar(ncid,fname,Nclass,[dimid,dimid2]) ;
    %
    %     end
    %
    %       % Leave define mode and enter data mode to write data.
    %       netcdf.endDef(ncid) ;
    %
    %       % Write data to variable.
    %       netcdf.putVar(ncid,varid,stringarray) ;
    %
  end                                                 % not cell
  
  [Nlen,Nwid] = size(sname.(fname)) ;
  
  if Nlen==0 || Nwid==0
    %skip
  else
    
    %if Nlen==1 && Nwid>1
    %  Nlen = Nwid ; Nwid=1;
    %end
    
    idim = find(Nlens==Nlen,1,'first') ;
    if ~isempty(idim)
      % We already got this dimension, so just use it
      dimname = ['dim' num2str(idim)] ;
      dimid   = netcdf.inqDimID(ncid,dimname) ;
    else
      % Get the dimensions
      dimcount=dimcount+1;
      dimname = ['dim' num2str(dimcount)];
      dimid = netcdf.defDim(ncid,dimname,Nlen) ;
      Nlens(dimcount) = Nlen ;
    end
    
    if Nwid>1
      idim = find(Nlens==Nwid,1,'first') ;
      if ~isempty(idim)
        % We already got this dimension, so just use it
        dimname2 = ['dim' num2str(idim)];
        dimid2   = netcdf.inqDimID(ncid,dimname2) ;
      else
        % Get the dimensions
        dimcount=dimcount+1;
        dimname2 = ['dim' num2str(dimcount)];
        dimid2 = netcdf.defDim(ncid,dimname2,Nwid) ;
        Nlens(dimcount) = Nwid ;
      end
      
      % Define a new variable in the file.
      % Python is opposite to matlab, so flip dimensions
      varid = netcdf.defVar(ncid,fname,Nclass,[dimid2,dimid]) ;
      
    else
      
      
      % Define a new variable in the file.
      varid = netcdf.defVar(ncid,fname,Nclass,dimid) ;
      
    end
    
    % Leave define mode and enter data mode to write data.
    netcdf.endDef(ncid) ;
    
    % Write data to variable: python is opposite to matlab, so flip
    % dimensions.
    netcdf.putVar(ncid,varid,sname.(fname)') ;
    
    
    % Get back into define mode for next loop through
    netcdf.reDef(ncid) ;
  end
end

netcdf.close(ncid);

