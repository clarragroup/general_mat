
function [cldLYR_wat,cldLYR_ice,cldTemp_wat,cldTemp_ice,...
  iTempWat,wTempWat,iTempIce,wTempIce, wLyrWat,wLyrIce,...
  isErr,errMsg] = ...
  cloudModel(liqbase,liqtop,icebase,icetop,zmflip,TEMPER,...
  ssp_watTemps, watTdependence,...
  ssp_iceTemps, iceTdependence,...
  layerModel,debugflag)
%
% Purpose: Select the index to model layers where cloud is desired
%   and get the mean cloud temperatures for the layers
%   for a variety of different cloud layering models. For now, models
%   cannot be mixed.  Also chooses weights to allow temperature-dependent
%   ssp files (modelled as a single cloud with interpolated ssps)and
%   allows for multiple habits (each habit is modelled as a separate cloud)
%
% Inputs:
%   liqbase,liqtop,icebase,icetop: cloud liquid and ice base and top:
%   zmflip:     model layer boundary heights, from highest to lowest
%   TEMPER: temperatures corresponding to zmflip
%   ssp_watTemps, ssp_iceTemps: temperatures of ssp files provided, 1 each
%   layerModel:  cloud model, current possibilities include:
%     'middleLayerMixedPhase': get single index to middle layer of
%        all liquid and ice layers
%     'eachLayerEachPhase': get indices for liquid and ice for all
%       layers between their respective bases and tops
%     'liquidTopped': get indices for liquid in top layer, ice
%       in all layers below (for single-phase clouds, the part of
%       the cloud whose phase is missing will be missing!)
%     'thinBoundaries': Optical depth decreases from cloud center
%       up to top and down to bottom
%   debugflag: create figures, for debugging
%
%   iceHabits: Coming soon! Habits for ssp ice files provided, 1 each
%
% Outputs:
%  cldLYR_wat,cldLYR_ice:   Indices to model layers with liquid and ice
%  cldTemp_wat,cldTemp_ice: Corresponding mean cloud temperatures
%  wLyrWat, wLyrIce:      Weights for optical depths in each layer (tot=1)
%  isErr,errMsg: error handling
%
% By Penny M. Rowe
%

if debugflag
  figure;
  plot(TEMPER,zmflip);
end

% Make sure heights go from top down
dz = diff(zmflip);
if find(dz>=0); error('One or more height does not decrease.'); end

isErr = 0; errMsg=[];

% .. Choose layers based on layer model
switch layerModel
  case 'middleLayerMixedPhase'
    % put the cloud in the middle layer
    base       = min(liqbase,icebase);
    top        = max(liqtop,icetop);
    htcld      = mean([base top]);
    cldLYR_wat = find(zmflip>htcld,1,'last') ;
    cldLYR_ice = cldLYR_wat ;
      
    % Get cloud temperature
    cldTemp_wat = mean([TEMPER(cldLYR_wat) TEMPER(cldLYR_wat+1)]);
    cldTemp_ice = cldTemp_wat ;

    % Layer weights (only 1 layer each)
    wLyrWat = 1 ;
    wLyrIce = 1 ;

  case 'eachLayerEachPhase'
    % Get layers between the liquid base and liquid top (cldLYR_wat),
    % get layers between the ice base and ice top (cldLYR_ice), and
    % get mean temps for each
    cldLYR_wat = find(zmflip>liqbase & zmflip<=liqtop) ;
    cldLYR_ice = find(zmflip>icebase & zmflip<=icetop) ;

    % Get cloud temperature
    cldTemp_wat = mean([TEMPER(cldLYR_wat) TEMPER(cldLYR_wat+1)],2);
    cldTemp_ice = mean([TEMPER(cldLYR_ice) TEMPER(cldLYR_ice+1)],2);

    % Layer weights: cloud is evenly distributed amongst all layers,
    % based on physical thickness
    dz_cldWat = zmflip(cldLYR_wat) - zmflip(cldLYR_wat+1) ;
    dz_cldIce = zmflip(cldLYR_ice) - zmflip(cldLYR_ice+1) ;
    wLyrWat   = dz_cldWat/sum(dz_cldWat) ;
    wLyrIce   = dz_cldIce/sum(dz_cldIce) ;

  case 'liquidTopped'
    % Put all the liquid in the top layer, put all the ice below
    % Warning: Not intended for single-phase or single-layer clouds!
    base       = min(liqbase,icebase);
    top        = max(liqtop,icetop);
    cldLYR     = find(zmflip>icebase & zmflip<=icetop) ;
    if length(cldLYR)==1;
      error('Multiple layers required for liquid-topped cloud');
    end
    cldLYR_wat = cldLYR(1) ;
    cldLYR_ice = cldLYR(2:end) ;

    % Get cloud temperature
    cldTemp_wat = mean([TEMPER(cldLYR_wat) TEMPER(cldLYR_wat+1)],2);
    cldTemp_ice = mean([TEMPER(cldLYR_ice) TEMPER(cldLYR_ice+1)],2);

    % Layer weights: cloud is evenly distributed amongst all layers,
    % based on physical thickness
    dz_cldWat = zmflip(cldLYR_wat) - zmflip(cldLYR_wat+1) ;
    dz_cldIce = zmflip(cldLYR_ice) - zmflip(cldLYR_ice+1) ;
    wLyrWat   = dz_cldWat/sum(dz_cldWat) ;
    wLyrIce   = dz_cldIce/sum(dz_cldIce) ;

   case 'thinBoundaries'
    % Get layers between the liquid base and liquid top (cldLYR_wat),
    % get layers between the ice base and ice top (cldLYR_ice), and
    % get mean temps for each
    cldLYR_wat = find(zmflip>liqbase & zmflip<=liqtop) ;
    cldLYR_ice = find(zmflip>icebase & zmflip<=icetop) ;

    % Get cloud temperature
    cldTemp_wat = mean([TEMPER(cldLYR_wat) TEMPER(cldLYR_wat+1)],2);
    cldTemp_ice = mean([TEMPER(cldLYR_ice) TEMPER(cldLYR_ice+1)],2);

    % Layer weights based on physical thickness:
    dz_cldWat  = zmflip(cldLYR_wat) - zmflip(cldLYR_wat+1) ;
    dz_cldIce  = zmflip(cldLYR_ice) - zmflip(cldLYR_ice+1) ;
    wLyrWatThk = dz_cldWat/sum(dz_cldWat) ;
    wLyrIceThk = dz_cldIce/sum(dz_cldIce) ;

    % Layer weights (decrease from middle out)
    cldLYR_wat_mid = (cldLYR_wat(end) - cldLYR_wat(1))/2 + cldLYR_wat(1) ;
    cldLYR_ice_mid = (cldLYR_ice(end) - cldLYR_ice(1))/2 + cldLYR_ice(1) ;
    wLyrWat = interp1([cldLYR_wat(end)+1 cldLYR_wat_mid cldLYR_wat(1)-1],[0 1 0],cldLYR_wat) ;
    wLyrIce = interp1([cldLYR_ice(end)+1 cldLYR_ice_mid cldLYR_ice(1)-1],[0 1 0],cldLYR_ice) ;

    % Correct for physical thickness
    wLyrWat = wLyrWat .* wLyrWatThk ;
    wLyrIce = wLyrIce .* wLyrIceThk ;

    % Normalize weights to sum of 1
    wLyrWat = wLyrWat / sum(wLyrWat) ;
    wLyrIce = wLyrIce / sum(wLyrIce) ;


  otherwise
    isErr  = 1;
    errMsg = 'Cloud model not found, check code.';
    error(errMsg);
    cldLYR_wat = -1;
    cldLYR_ice = -1;
end

if (isnan(cldLYR_wat) || isnan(cldLYR_ice)) || ...
    (isempty(cldLYR_wat) && isempty(cldLYR_wat))
  isErr  = 1;
  errMsg = 'No cloud found.';
  cldLYR_wat = -1;
  cldLYR_ice = -1;
end



% .. Temperature dependence of ssp data: water and ice
%    Changed to get all temps for liquid and ice
%    (cldTemp changed to cldTemp_wat and cldTemp_ice), also
%    interp1_weights2 renamed to interp1_weights, PMR, 2016/03/19
[iTempWat,wTempWat] = ...
  interp1_weights(ssp_watTemps,cldTemp_wat,watTdependence);
[iTempIce,wTempIce] = ...
  interp1_weights(ssp_iceTemps,cldTemp_ice,iceTdependence);



if debugflag
  disp([(1:length(zmflip)-1)' zmflip(1:length(zmflip)-1) zmflip(2:length(zmflip))]);
  disp(cldLYR_wat);
  disp([(1:length(zmflip)-1)' TEMPER(1:length(zmflip)-1) TEMPER(2:length(zmflip))]);
  disp(cldLYR_wat);
  disp(cldTemp_wat);
end


    